package Protocolo;

public class I2C extends Protocolo {

	private int velocidadMb = 5;

	public String getNombre() {
		return "I2C";
	}

	public int getVelocidad() {
		return velocidadMb;
	}

}
