import Modulos.Modulo;
import Protocolo.Protocolo;

public class FabricaMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		
		FabricaModulos modulos= new FabricaModulos();
		FabricaProtocolos protocolos= new FabricaProtocolos();
		
		
		//Se crea el modulo wifi
		Modulo wifi= modulos.cargarModulo("Wifi");
		wifi.setIp("192.168.0.200");		
		
		//Se creal el protocolo I2C
		Protocolo protocoloI2C= protocolos.crearProtocolo("RS485");
		
		//Se vincula el protocolo I2C con el modulo wifi
		wifi.setProtocoloUsado(protocoloI2C);
		
		//Impresion de datos
		System.out.println("**************** \n"+wifi.getDatosGenerales());
		
		
		
		//Otro ejemplo con ethernet
		Modulo ethernet= modulos.cargarModulo("Ethernet");
		ethernet.setIp("192.168.0.5");		

		Protocolo protocoloSPI= protocolos.crearProtocolo("SPI");
		ethernet.setProtocoloUsado(protocoloSPI);
		
		System.out.println("**************** \n"+ethernet.getDatosGenerales());
		
		
	}

}
