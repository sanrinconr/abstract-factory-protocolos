package Protocolo;

public class RS485 extends Protocolo {

	private int velocidadMb = 10;

	public String getNombre() {
		return "RS485";
	}

	public int getVelocidad() {
		return velocidadMb;
	}

}
