package Modulos;

import Protocolo.Protocolo;

public class Wifi extends Modulo {
	private String modulo="Wifi";
	private String ip;
	private String protocoloUsado="No definido";
	private int velocidadMaxima;

	public void setIp(String ip) {
		this.ip=ip;
	}
	public String getIp() {
		return ip;
	}
	public String getNombre() {
		return modulo;
	}
	public void setProtocoloUsado(Protocolo protocolo) {
		protocoloUsado=protocolo.getNombre();
		velocidadMaxima=protocolo.getVelocidad();
		
	}
	
	public String getProtocoloUsado() {
		return protocoloUsado;
	}
	@Override
	public String getDatosGenerales() {
		
		return "Modulo: "+modulo+"\n"
					+ "Protocolo: "+protocoloUsado+"\n"
						+ "Ip: "+ip+"\n"
								+ "Velocidad maxima (mb/s): "+velocidadMaxima;
	}
	
}
