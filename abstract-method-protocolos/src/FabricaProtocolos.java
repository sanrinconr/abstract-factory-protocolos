import Protocolo.*;

public class FabricaProtocolos {
	
	
	public Protocolo crearProtocolo(String protocolo) {
		switch(protocolo) {
		case "SPI":
			return new SPI();
		case "RS485":
			return new RS485();
		case "I2C":
			return new I2C();
		}
		
		return null;
		
	}
	
}
