package Protocolo;

public class SPI extends Protocolo {

	private int velocidadMb = 10;

	public String getNombre() {
		return "SPI";
	}

	public int getVelocidad() {
		return velocidadMb;
	}

}
