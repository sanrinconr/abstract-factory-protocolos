package Modulos;

import Protocolo.Protocolo;

public class Ethernet extends Modulo {
	private String modulo="Ethernet";
	private String ip;
	private String protocoloUsado="No definido";
	private int velocidadMaxima;

	public void setIp(String ip) {
		this.ip=ip;
	}
	public String getIp() {
		return ip;
	}
	
	public String getNombre() {
		return "Nombre";
	}
	public void setProtocoloUsado(Protocolo protocolo) {
		protocoloUsado=protocolo.getNombre();
		velocidadMaxima=protocolo.getVelocidad();
		
	}
	
	public String getProtocoloUsado() {
		return protocoloUsado;
	}
public String getDatosGenerales() {
		
		return "Modulo: "+modulo+"\n"
					+ "Protocolo: "+protocoloUsado+"\n"
						+ "Ip: "+ip+"\n"
								+ "Velocidad maxima (mb/s): "+velocidadMaxima;
	}
}
